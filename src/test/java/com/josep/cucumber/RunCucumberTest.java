package com.josep.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {
    "pretty",
    "json:build/cucumber.json",
    "html:build/result",
    "rerun:rerun/failed_scenarios.txt",
    "junit:target/cucumber-reports/cucumber.xml"
},
    publish = true,
    glue = "classpath:com.josep.cucumber"
)
public class RunCucumberTest {

}
